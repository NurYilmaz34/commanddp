﻿using UnityEngine;
using UnityEngine.UI;

public class ClientState : MonoBehaviour
{
    private StateOn stateOn;
    private StateOff stateOff;
    private StateSave stateSave;
    private ForwarderState forwarder;

    public Button onButton;
    public Button offButton;
    public Button saveButton;

    private void Awake()
    {
        stateOn = new StateOn(new States());
        stateOff = new StateOff(new States());
        stateSave = new StateSave(new States());

        forwarder = new ForwarderState();
    }

    private void Start()
    {
        onButton.onClick.AddListener(On);
        offButton.onClick.AddListener(Off);
        saveButton.onClick.AddListener(Save);
    }

    private void On()
    {
        forwarder.Execute(stateOn);
    }

    private void Off()
    {
        forwarder.Execute(stateOff);
    }

    private void Save()
    {
        forwarder.Execute(stateSave);
    }
}
