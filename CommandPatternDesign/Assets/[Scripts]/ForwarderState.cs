﻿using UnityEngine;

public class ForwarderState : MonoBehaviour
{
    public void Execute(ICommand command)
    {
        command.Execute();
    }
}
