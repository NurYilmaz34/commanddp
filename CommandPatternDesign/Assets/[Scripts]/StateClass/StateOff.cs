﻿using UnityEngine;

public class StateOff : ICommand
{
    public States states;

    public StateOff(States stOff)
    {
        this.states = stOff;
    }

    public void Execute()
    {
        states.GetStateOff();
    }
}
