﻿using UnityEngine;

public class StateOn : ICommand
{
    public States states;

    public StateOn(States stOn)
    {
        this.states = stOn;
    }

    public void Execute()
    {
        states.GetStateOn();
    }
}
