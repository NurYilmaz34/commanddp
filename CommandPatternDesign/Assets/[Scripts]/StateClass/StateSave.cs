﻿using UnityEngine;

public class StateSave : ICommand
{
    public States states;

    public StateSave(States stSave)
    {
        this.states = stSave;
    }

    public void Execute()
    {
        states.GetStateSave();
    }
}
